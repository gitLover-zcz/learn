package com.learn.mapper;

import com.learn.pojo.DbStu;
import org.springframework.stereotype.Repository;

@Repository
public interface DbStuMapperCustom {

    DbStu getStuById(String sid);

}