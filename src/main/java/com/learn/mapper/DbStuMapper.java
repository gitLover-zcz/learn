package com.learn.mapper;

import com.learn.my.mapper.MyMapper;
import com.learn.pojo.DbStu;
import org.springframework.stereotype.Repository;

@Repository
public interface DbStuMapper extends MyMapper<DbStu> {
}