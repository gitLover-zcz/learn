package com.learn.pojo.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
public class DBStuBO {
    private String id;
    @NotBlank
    private String name;
    @NotNull
    private Integer sex;
    @Min(value = 1, message = "年级最小为1")
    @Max(value = 6, message = "年级最大为6")
    private Integer grade;
    @Range(min = 1, max = 18, message = "班级区间1~18")
    private Integer classRoom;
    @Length(min = 3, max = 8, message = "英文名长度区间3~8")
    private String englishName;
    @Size(min = 2, max = 5, message = "技能填写最少2，做多5个")
    private List<String> skill;
    @Email(message = "邮箱格式不正确")
    private String email;
}
