package com.learn;

import com.learn.pojo.Stu;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringBootConfig {
    @Bean
    public Stu stu() {
        return new Stu("zcz", 20);
    }
}
