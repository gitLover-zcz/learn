package com.learn.controller;

import com.learn.pojo.MyConfig;
import com.learn.pojo.Stu;
import com.learn.utils.JSONResult;
import com.learn.utils.MyAsyncTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
@Slf4j
public class HelloController {

    @GetMapping("hello")
    public String hello() {
        return "Hello Learn~";
    }

    @Autowired
    private Stu stu;

    @GetMapping("getStu")
    public Object getStu() {
        return stu;
    }

    @Autowired
    private MyConfig myConfig;
    @Autowired
    private MyAsyncTask myAsyncTask;

    @GetMapping("getMyConfig")
    public Object getMyConfig() {
        myAsyncTask.asyncTask();
        log.info("这是跳过异步处理的结果");
        return myConfig;
    }

    @GetMapping("getStudent")
    public JSONResult getStudent() {
        Stu student = new Stu();
        student.setName("student");
        student.setAge(18);

        log.debug(student.toString());
        log.info(student.toString());
        log.warn(student.toString());
        log.error(student.toString());

//        return JSONResult.errorMsg("调用接口错误");
        return JSONResult.ok(student);
    }

    @Value("${app.name.xxx.yyy.zzz}")
    private String zzz;
    @Value("${app.name.xxx.yyy.hhh}")
    private String hhh;
    @Value("${app.name.xxx.yyy.iii}")
    private String iii;
    @Value("${app.name.xxx.yyy.port}")
    private String port;

    @GetMapping("getYml")
    public Object getYml() {
        return zzz + "\n" + hhh + ":" + port + "\n" + iii;
    }

    @PostMapping("upload")
    public String upload(MultipartFile file) {

        try {
            file.transferTo(new File("D:\\Temp\\" + file.getOriginalFilename()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "上传成功";
    }
}
