package com.learn.controller;

import com.learn.pojo.Stu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.servlet.http.HttpServletRequest;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

@Controller
@Slf4j
@RequestMapping("thyme")
public class ThymeleafController {

    @GetMapping("hello")
    public String hello(Model model, HttpServletRequest request) {
        String jack = "jack";
        model.addAttribute("there", jack);
        Date birthday = new Date();
        model.addAttribute("birthday", birthday);
        Integer sex = 2;
        model.addAttribute("sex", sex);
        List<String> list = new ArrayList<>();
        list.add("java");
        list.add("ios");
        list.add("android");
        list.add("html");
        list.add("css");
        model.addAttribute("list", list);
        Map<String, Object> map = new HashMap<>();
        map.put("id", "10010");
        map.put("sex", "男");
        map.put("list", list);
        model.addAttribute("map", map);

        request.setAttribute("englishName", "abcxyz");
        request.getSession().setAttribute("userToken", "fjld-dfsdjfk-dfdsf-fsdf");

        return "teacher";
    }

    @Autowired
    private TemplateEngine templateEngine;

    @GetMapping("createHTML")
    @ResponseBody
    public String createHTML() throws IOException {
        String jack = "jack";

        Date birthday = new Date();

        Integer sex = 2;

        List<String> list = new ArrayList<>();
        list.add("java");
        list.add("ios");
        list.add("android");
        list.add("html");
        list.add("css");

        Map<String, Object> map = new HashMap<>();
        map.put("id", "10010");
        map.put("sex", "男");
        map.put("list", list);

        // 生成context
        Context context = new Context();
        context.setVariable("there", jack);
        context.setVariable("birthday", birthday);
        context.setVariable("sex", sex);
        context.setVariable("list", list);
        context.setVariable("map", map);

        // 输出文件路径
        String path = "D:\\Temp";

        Writer out = new FileWriter(path + "\\teacher.html");

        templateEngine.process("teacher", context, out);
        out.close();

        return "ok";
    }

}
