package com.learn.controller;

import com.learn.pojo.DbStu;
import com.learn.pojo.bo.DBStuBO;
import com.learn.service.StuService;
import com.learn.utils.JSONResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("dbStu")
public class DBStuController {

    @Autowired
    private StuService stuService;

    @GetMapping("{stuId}/get")
    public String getStu(@PathVariable("stuId") String stuId,
                      @RequestParam("id") String id,
                      String name) {

        /**
         * @RequestParam： 用于获取url中的请求参数，如果参数变量名保持一致，该注解可以省略
         */

        log.info("stuId:" + stuId);
        log.info("id:" + id);
        log.info("name:" + name);

        return "查询stu~";
    }

    @GetMapping("/get")
    public JSONResult getStu(String id) {
//        DbStu dbStu = stuService.queryById(id);
//        List<DbStu> dbStus = stuService.queryByCondition2(name, sex);
        DbStu dbStu = stuService.queryByIdCustom(id);
        return JSONResult.ok(dbStu);
    }

    @PostMapping("create")
    public JSONResult createStu() {
        stuService.saveStu(new DbStu());
        return JSONResult.ok();
    }

    @PostMapping("create2")
    public JSONResult createStu2(@Valid @RequestBody DBStuBO stuBO, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> errors = getErrors(result);
            return JSONResult.errorMap(errors);
        }
        stuService.saveStu2(stuBO);
        return JSONResult.ok();
    }

    @PutMapping("update")
    public JSONResult updateStu(@RequestBody DbStu stu) {
        stuService.updateStu(stu);
        return JSONResult.ok();
    }

    @DeleteMapping("delete")
    public JSONResult deleteStu(@RequestBody DbStu stu) {
        stuService.deleteStu(stu);
        return JSONResult.ok("删除成功");
    }

    private Map<String, String> getErrors(BindingResult result) {
        Map<String, String> map = new HashMap<>();
        List<FieldError> fieldErrors = result.getFieldErrors();
        for (FieldError fieldError : fieldErrors) {
            String field = fieldError.getField();
            String defaultMessage = fieldError.getDefaultMessage();
            map.put(field, defaultMessage);
        }
        return map;
    }

    @GetMapping("testTrans")
    public JSONResult testTrans() {
        stuService.testTrans();
        return JSONResult.ok();
    }
}
