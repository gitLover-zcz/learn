package com.learn.controller.interceptor;

import com.learn.enums.ErrorCodeEnum;
import com.learn.exception.GraceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class UserInfoInterceptor implements HandlerInterceptor {

    /**
     * 拦截请求，访问到controller之前
     * @param request
     * @param response
     * @param handler
     * @return
     *      false：请求拦截，true：请求放行，可以继续访问后面的controller
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String userId = request.getHeader("userId");
        String userToken = request.getHeader("userToken");

        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(userToken)) {
            log.error(ErrorCodeEnum.NO_USER_INFO);
            GraceException.display(ErrorCodeEnum.NO_USER_INFO);
            return false;
        }

        // 假设真实用户的ID是1001，用户token是abcxyz
        if (!"1001".equalsIgnoreCase(userId) || !"abcxyz".equalsIgnoreCase(userToken)) {
            log.error(ErrorCodeEnum.NO_USER_AUTH);
            GraceException.display(ErrorCodeEnum.NO_USER_AUTH);
            return false;
        }

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    /**
     * 访问到controller之后，渲染视图之前
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    /**
     * 访问到controller之后，渲染视图之后
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
