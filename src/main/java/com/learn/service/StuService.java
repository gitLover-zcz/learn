package com.learn.service;

import com.learn.pojo.DbStu;
import com.learn.pojo.bo.DBStuBO;

import java.util.List;

public interface StuService {

    /**
     * 新增stu到数据库
     * @param stu
     */
    void saveStu(DbStu stu);
    /**
     * 新增stu到数据库2
     * @param stuBO
     */
    void saveStu2(DBStuBO stuBO);

    /**
     * 根据主键查询对象
     * @param id
     * @return
     */
    DbStu queryById(String id);
    DbStu queryByIdCustom(String id);

    /**
     * 根据条件查询结果集
     * @param name
     * @param sex
     * @return
     */
    List<DbStu> queryByCondition(String name, Integer sex);
    /**
     * 根据条件查询结果集2
     * @param name
     * @param sex
     * @return
     */
    List<DbStu> queryByCondition2(String name, Integer sex);

    /**
     * 更新对象
     * @param stu
     */
    void updateStu(DbStu stu);

    /**
     * 删除对象
     * @param stu
     */
    void deleteStu(DbStu stu);

    /**
     * 测试事务
     */
    void testTrans();
}
