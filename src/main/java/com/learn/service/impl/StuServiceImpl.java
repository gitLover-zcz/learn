package com.learn.service.impl;

import com.learn.mapper.DbStuMapper;
import com.learn.mapper.DbStuMapperCustom;
import com.learn.pojo.DbStu;
import com.learn.pojo.bo.DBStuBO;
import com.learn.service.StuService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.UUID;

@Service
public class StuServiceImpl implements StuService {

    @Autowired
    private DbStuMapper stuMapper;
    @Autowired
    private DbStuMapperCustom stuMapperCustom;

    @Override
    public void saveStu(DbStu stu) {
        String sid = StringUtils.replace(UUID.randomUUID().toString(),"-","");
        stu.setId(sid);
        stu.setName("慕课网");
        stu.setSex(1);
        stuMapper.insert(stu);
    }

    @Override
    public void saveStu2(DBStuBO stuBO) {
        String sid = StringUtils.replace(UUID.randomUUID().toString(),"-","");
        DbStu stu = new DbStu();
        BeanUtils.copyProperties(stuBO, stu);
        stu.setId(sid);
        stuMapper.insert(stu);
    }

    @Override
    public DbStu queryById(String id) {
        DbStu dbStu = stuMapper.selectByPrimaryKey(id);
        return dbStu;
    }

    @Override
    public DbStu queryByIdCustom(String id) {
//        try {
//            Thread.sleep(3500);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        DbStu dbStu = stuMapperCustom.getStuById(id);
        return dbStu;
    }

    @Override
    public List<DbStu> queryByCondition(String name, Integer sex) {
        Example example = new Example(DbStu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("name", name);
        criteria.andEqualTo("sex", sex);
        List<DbStu> dbStus = stuMapper.selectByExample(example);
        return dbStus;
    }

    @Override
    public List<DbStu> queryByCondition2(String name, Integer sex) {
        DbStu dbStu = new DbStu();
        dbStu.setName(name);
        dbStu.setSex(sex);
        List<DbStu> dbStus = stuMapper.select(dbStu);
        return dbStus;
    }

    @Override
    public void updateStu(DbStu stu) {
        stuMapper.updateByPrimaryKey(stu);
    }

    @Override
    public void deleteStu(DbStu stu) {
//        stuMapper.deleteByPrimaryKey(stu);
//        stuMapper.delete(stu);
        Example example = new Example(DbStu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("name", stu.getName());
        criteria.andEqualTo("sex", stu.getSex());
        stuMapper.deleteByExample(example);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void testTrans() {
        DbStu stu = new DbStu();
        stu.setId("3");
        stu.setName("newInsert");
        stu.setSex(9);
        stuMapper.insert(stu);

//        int i = 100 / 0;

        DbStu dbStu = new DbStu();
        dbStu.setId("3");
        dbStu.setName("updateData");
        dbStu.setSex(0);
        stuMapper.updateByPrimaryKey(dbStu);

    }
}
