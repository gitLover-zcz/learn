package com.learn.enums;

import com.learn.annotation.ErrorDescription;

public class ErrorCodeEnum {

    @ErrorDescription("用户校验不通过，信息不完整")
    public static final String NO_USER_INFO = "1001";
    @ErrorDescription("用户权限不通过")
    public static final String NO_USER_AUTH = "1002";

}
