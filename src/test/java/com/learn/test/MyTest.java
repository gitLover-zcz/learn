package com.learn.test;

import com.learn.pojo.DbStu;
import com.learn.service.impl.StuServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest  // 被springboot加载为测试类
public class MyTest {

    @Autowired
    private StuServiceImpl stuService;

    @Test
    public void testSaveStu() {
        stuService.saveStu(new DbStu());
    }

    @Test
    public void updateStu() {
        DbStu stu = new DbStu();
        stu.setId("2");
        stu.setName("444");
        stu.setSex(9);
        stuService.updateStu(stu);
    }

    @Test
    public void deleteStu() {
        DbStu stu = new DbStu();
        stu.setId("2");
        stuService.deleteStu(stu);
    }

    @Test
    public void testTrans() {
        stuService.testTrans();
    }

}
